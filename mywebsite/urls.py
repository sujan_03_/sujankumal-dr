from django.urls import path
from mywebsite import views
# from AppOne.views import SchoolList

app_name = 'MyWebsite'

urlpatterns = [
    path('', views.index , name="index"),
    path('game01/', views.game01, name='game01'),
    path('api/game/', views.game_api),
]