from django.shortcuts import render, HttpResponse, redirect

from django.contrib.auth import authenticate, login, logout
from django.http import JsonResponse, Http404
from django.views.decorators.csrf import csrf_exempt
from rest_framework.parsers import JSONParser
from rest_framework.views import APIView
from rest_framework import status
from django.contrib import messages
from django.contrib.auth.decorators import login_required
import random


# Create your views here.
def index(request):
    return render(request, 'mywebsite/index.html')

def game01(request):
    return render(request, 'mywebsite/game01.html')

@csrf_exempt
def game_api(request):

    if request.method == 'GET':
        return JsonResponse("", safe=False)
    else:
        data = JSONParser().parse(request)
        stage = data.get('stage')
        positions = data.get('positions')
        new_positions =  getPositions(stage, positions)
        number_of_ones = new_positions.count(1)
        while number_of_ones < 1:
            new_positions = getPositions(stage, positions)
            number_of_ones = new_positions.count(1)
            print("positions: ", new_positions)
        return JsonResponse(new_positions, status=200, safe=False)

def getPositions(stage, positions):
    new_positions = [random.randrange(0, 2) for i in range((stage) ** 2)]
    return new_positions
