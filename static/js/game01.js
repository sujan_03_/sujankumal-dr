function Square(props) {
  return (
    <button className="square" onClick={props.onClick}>
        {props.value}
    </button>
  );
}

class Board01 extends React.Component{
    constructor(props) {
        super(props);
    }
    renderSquare(item, key){
        console.log("renderSquare: ", item , key);
        return(
            <Square
                key = {key}
                value={this.props.squares[key]}
                onClick={()=>this.props.onClick(key)}
            />
        );
    }

    render() {
        return (
                this.props.squares.map((item, key)=>{
                        return this.renderSquare(item,key)
                    })
        );
    }

}


class Game01 extends React.Component{

    noOfOnesFound = 0;

    constructor(props) {
        super(props);
        this.state = {
            history: [{
                    squares:Array(1).fill('X')
                }],
            stepNumber:0,
            stage:1,
            positions:[],
            countOfOnes:0,
            win:null,
        }
        this.fetchdata(1,[],[]);
    }

    postData = async (url = '', data = {}) => {
          // Default options are marked with *
          const response = await fetch(url, {
            method: 'POST', // *GET, POST, PUT, DELETE, etc.
            mode: 'cors', // no-cors, *cors, same-origin
            cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
            credentials: 'same-origin', // include, *same-origin, omit
            headers: {
              'Content-Type': 'application/json'
              // 'Content-Type': 'application/x-www-form-urlencoded',
            },
            redirect: 'follow', // manual, *follow, error
            referrer: 'no-referrer', // no-referrer, *client
            body: JSON.stringify(data) // body data type must match "Content-Type" header
          });
          return await response.json(); // parses JSON response into native JavaScript objects
        };
    async componentDidMount() {
    }

    async fetchdata(stage, positions, history){
        try{
            let data = await this.postData('http://localhost:8000/api/game/', { 'stage': stage, 'positions': positions, 'history': history });
            let squares =  JSON.parse(JSON.stringify(data));
            this.setState({
                history: [{
                    squares : Array(squares.length).fill('X'),
                }],
                positions: squares,
                countOfOnes: squares.reduce((total, value) => total+ value ,0),
            });
                console.log("componentDidMount, response.json", JSON.stringify(data), this.state.history); // JSON-string from `response.json()` call

        }catch (e) {
            console.error('Error',e);
        }
    }
    handleClick(i){
        const history = this.state.history.slice(0,
            this.state.stepNumber +1);
        const current = history[history.length - 1];
        const squares = current.squares.slice();
        console.log("Before handle Click: ", this.state.stepNumber, this.state.countOfOnes, this.noOfOnesFound);

        // if (this.state.countOfOnes == this.state.stepNumber){
        //     console.log("out of moves", this.state.stepNumber, this.state.countOfOnes, this.noOfOnesFound);
        //     return;
        // }
        if (squares[i] == '1' | squares[i] == '0' ){
            console.log("Clicked same square");
            return;
        }
        if (this.state.positions[i] == 1){
            squares[i] = '1';
            this.noOfOnesFound++;
        }else {
            squares[i] = '0';
        }
        this.setState({
          history: history.concat([{
            squares: squares,
          }]),
          stepNumber:history.length,
        });
        console.log("After handle Click: ", this.state.stepNumber, this.state.countOfOnes, this.noOfOnesFound);

    }
    checkWin(){
        if (this.state.countOfOnes == this.noOfOnesFound){
            console.log('winner');
            return 1;
        }
    }
    jumpTo(step) {
        this.setState({
          stepNumber: step,
        });
    }
    nextStage(){
        let next_stage = this.state.stage+1;
        this.setState({
            stepNumber:0,
            stage: next_stage,
        });
        this.fetchdata(next_stage, this.state.positions, this.state.history )
        this.noOfOnesFound = 0;
        console.log("stage: ", next_stage);
    }
    previousStage(){
        this.setState({
            stage: this.state.stage-1,
        });
    }

    render() {
        let status, next =null;
        const history = this.state.history;
        const current = history[this.state.stepNumber];
        console.log("game render: history, current: ", this.state.history, current);

        // const winner = calculateWinner(current.squares);
        const moves = history.map((step, move) => {
          const desc = move ?
            'Go to move #' + move :
            'Go to game start';
          return (
            <li key={move}>
              <button onClick={() => this.jumpTo(move)}>{desc}</button>
            </li>
          );
        });

        if (this.checkWin()) {
            status = 'Won';
            next = <button onClick={()=> this.nextStage()}>Next Stage</button>;
        } else {
        }
        return (
            <div className="game01">
                <table>
                    <thead><tr><td>Game 01</td></tr></thead>
                    <tbody ><tr><td>
                                <div className="board">
                                    <Board01
                                        stage = {this.state.stage}
                                        squares =  {current.squares}
                                        onClick = {(i) => this.handleClick(i)}
                                    />
                                </div>

                            </td>
                            <td>
                                <div>
                                        <ol>{ moves}</ol>
                                    </div>
                    </td></tr></tbody>
                    <tfoot><tr><td>
                    <div className="game-info">
                        <div className="game-status">{ status}</div>
                        <div className="next-stage">{next }</div>
                        <div className="previous-stage">
                            <button>Previous</button>
                        </div>
                    </div>
                    </td></tr></tfoot>

                </table>

            </div>
        );
    }

}


ReactDOM.render(<Game01/>, document.getElementById('game01'));