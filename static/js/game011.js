
class Board01 extends React.Component{

    constructor(props) {
    super(props);
    console.log("Board01 constructor, props.stage ",props.stage);
    this.state = {
        // squares: Array(props.stage**2).fill(null),
        positions: Array(props.stage**2).fill(null),
        };
    }
    renderSquare(squarekey) {
        return (
            <button
                key ={squarekey}
                value={this.props.positions[squarekey]}
                className="square01"
                onClick={()=>this.props.onClick(squarekey)}
            >{this.props.positions[squarekey]}</button>
        );
    }
    renderBoard(){
        const items =[];
        var squareKey = 1;
        for (let i = 0 ;i <= this.props.stage; i++){
            let row = []
            for (let j = 0; j<= this.props.stage; j++){
                // row.push(<span className='square01' key={squareKey}>{squareKey}</span>);
                row.push(this.renderSquare(squareKey))
                squareKey+=2;
            }
            items.push(<div key={i}>{row}</div>);
        }
        console.log("renderBoard, items ", items)
        return items;
    }

    render() {
        return(
            <div>
                {this.renderBoard()}
            </div>
        )
    }
}
class Game01 extends React.Component{
    st = 3;

    constructor(props) {
        super(props);
        this.state = {
            stage:this.st,
            // positions:[0],
            stepNumber:0,
            history: [{
                positions:Array(this.st**2).fill(null),
            }],
        };
    }
    postData = async (url = '', data = {}) => {
          // Default options are marked with *
          const response = await fetch(url, {
            method: 'POST', // *GET, POST, PUT, DELETE, etc.
            mode: 'cors', // no-cors, *cors, same-origin
            cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
            credentials: 'same-origin', // include, *same-origin, omit
            headers: {
              'Content-Type': 'application/json'
              // 'Content-Type': 'application/x-www-form-urlencoded',
            },
            redirect: 'follow', // manual, *follow, error
            referrer: 'no-referrer', // no-referrer, *client
            body: JSON.stringify(data) // body data type must match "Content-Type" header
          });
          return await response.json(); // parses JSON response into native JavaScript objects
        };


    async componentDidMount() {
    try {
            let data = await this.postData('http://localhost:8000/api/game/', { 'stage': this.state.stage, 'positions':"" });
            // console.log("componentDidMount, state.position", this.state.history);
            this.setState({

                positions:JSON.stringify(data),
            })
            console.log("componentDidMount, response.json", JSON.stringify(data), this.state.positions); // JSON-string from `response.json()` call
         }catch (e) {
            console.error('Error',e);
        }
    }
    async componentDidUpdate(){

    }

    handleClick(squareKey){
        console.log("handleCLick, squarekey",squareKey);
        const history = this.state.history.slice(0,this.state.stepNumber+1);
        const current = history[history.length - 1];
        const positions = current.positions.slice();
        console.log("history, current, positions:", history,current,positions);
        positions[squareKey] = this.state.positions[squareKey-2]?'1':'0';
        this.setState({
            history: history.concat([{
                positions: positions,
            }]),
            stepNumber:history.length,
            positions:positions,
            stage:this.st,
        });

    }
    render() {
        return (
            <div className="Game01Board">
                <Board01
                    stage = {this.state.stage}
                    positions = {this.state.history}
                    onClick={(squareKey)=>this.handleClick(squareKey)}
                />
            </div>
        );
    }
}
ReactDOM.render(<Game01/>, document.getElementById('game01'))
